﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MentorTracker.Data
{
    public class Duty
    {
        private readonly String _id;
        private readonly String _name;
        private readonly DutyType _type;
        private readonly int _level;
        public List<String> _tags = new();

        public string Id { get => _id;}

        public string Name => _name;

        public DutyType Type => _type;

        public int Level => _level;

        public Duty(string id, string name, DutyType type, int level, List<string> tags)
        {
            _id = id;
            _name = name;
            _type = type;
            _level = level;
            _tags = tags;
        }

        public override bool Equals(object? obj)
        {
            return obj is Duty duty &&
                   Id == duty.Id;
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
    }
}
