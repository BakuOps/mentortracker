﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace MentorTracker.Data
{
    public enum DutyType
    {
        [Description("Dungeon")]
        DUNGEON,
        [Description("Trial")]
        TRIAL,
        [Description("Extreme Trial")]
        EXTREME_TRIAL,
        [Description("Normal Raid")]
        NORMAL_RAID,
        [Description("Alliance Raid")]
        ALLIANCE_RAID,
        [Description("Guildhest")]
        GUILDHEST
    }

    public static class DutyTypeExtension
    {
        public static string ToDescriptionString(this DutyType val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
