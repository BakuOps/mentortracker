﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MentorTracker.Data
{
    public class DutyListHelper
    {
        public static List<Duty> GetDuties()
        {
            List<Duty> duties = new()
            {
                //----- Dungeons -----
                new Duty("dungen", "Dungeon", DutyType.DUNGEON, 1, new List<string>(new string[] {})),

                //--- ARR ---
                new Duty("dunsas", "Sastasha", DutyType.DUNGEON, 15, new List<string>(new string[] {})),
                new Duty("duntam", "The Tam-Tara Deepcroft", DutyType.DUNGEON, 16, new List<string>(new string[] {})),
                new Duty("duncop", "Copperbell Mines", DutyType.DUNGEON, 17, new List<string>(new string[] {})),

                //----- Guildhests -----
                new Duty("guhgen", "Guildhest", DutyType.GUILDHEST, 1, new List<string>(new string[] {})),
                new Duty("guhbep", "Basic Training: Enemy Parties", DutyType.GUILDHEST, 10, new List<string>(new string[] {})),
                new Duty("guhuda", "Under the Armor", DutyType.GUILDHEST, 10, new List<string>(new string[] {})),
                new Duty("guhbes", "Basic Training: Enemy Strongholds", DutyType.GUILDHEST, 15, new List<string>(new string[] {})),

                //----- Trials -----
                new Duty("trigen", "Trial", DutyType.TRIAL, 1, new List<string>(new string[] {})),

                //--- ARR ---
                new Duty("triboe", "The Bowl of Embers", DutyType.TRIAL, 20, new List<string>(new string[] {})),
                new Duty("trinav", "The Navel", DutyType.TRIAL, 34, new List<string>(new string[] {})),
                new Duty("trihow", "The Howling Eye", DutyType.TRIAL, 44, new List<string>(new string[] {})),

                //----- Extreme-Trials -----
                new Duty("extgen", "Trial", DutyType.EXTREME_TRIAL, 1, new List<string>(new string[] {})),

                //--- ARR ---
                new Duty("extult", "The Minstrel's Ballad: Ultima's Bane", DutyType.EXTREME_TRIAL, 50, new List<string>(new string[] {})),
                new Duty("exthow", "The Howling Eye (Extreme)", DutyType.EXTREME_TRIAL, 50, new List<string>(new string[] {})),
                new Duty("extnav", "The Navel (Extreme)", DutyType.EXTREME_TRIAL, 50, new List<string>(new string[] {})),

                //----- Alliance Raids -----
                new Duty("alrgen", "Alliance Raid", DutyType.ALLIANCE_RAID, 1, new List<string>(new string[] {})),

                //--- ARR ---
                new Duty("alrlot", "The Labyrinth of the Ancients", DutyType.ALLIANCE_RAID, 50, new List<string>(new string[] {})),
                new Duty("alrsyr", "Syrcus Tower", DutyType.ALLIANCE_RAID, 50, new List<string>(new string[] {})),
                new Duty("alrwod", "The World of Darkness", DutyType.ALLIANCE_RAID, 50, new List<string>(new string[] {})),

                //----- Normal Raids -----
                new Duty("norgen", "Normal Raid", DutyType.NORMAL_RAID, 1, new List<string>(new string[] {})),

                //--- HW ---
                new Duty("nora01", "Alexander - The Fist of the Father", DutyType.NORMAL_RAID, 60, new List<string>(new string[] {})),
                new Duty("nora02", "Alexander - The Cuff of the Father", DutyType.NORMAL_RAID, 60, new List<string>(new string[] {})),
                new Duty("nora03", "Alexander - The Arm of the Father", DutyType.NORMAL_RAID, 60, new List<string>(new string[] {})),
                new Duty("nora04", "Alexander - The Burden of the Father", DutyType.NORMAL_RAID, 60, new List<string>(new string[] {})),
            };

            return duties;
        }
    }
}
